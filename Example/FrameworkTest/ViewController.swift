//
//  ViewController.swift
//  FrameworkTest
//
//  Created by Daniel Salhuana on 10/07/2021.
//  Copyright (c) 2021 Daniel Salhuana. All rights reserved.
//

import UIKit
import FrameworkTest

class ViewController: UIViewController {
    
    var testFunction = BCPTestFunction()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        testFunction.load(message: "Esto es una prueba")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

