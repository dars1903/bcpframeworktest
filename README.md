# FrameworkTest

[![CI Status](https://img.shields.io/travis/Daniel Salhuana/FrameworkTest.svg?style=flat)](https://travis-ci.org/Daniel Salhuana/FrameworkTest)
[![Version](https://img.shields.io/cocoapods/v/FrameworkTest.svg?style=flat)](https://cocoapods.org/pods/FrameworkTest)
[![License](https://img.shields.io/cocoapods/l/FrameworkTest.svg?style=flat)](https://cocoapods.org/pods/FrameworkTest)
[![Platform](https://img.shields.io/cocoapods/p/FrameworkTest.svg?style=flat)](https://cocoapods.org/pods/FrameworkTest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FrameworkTest is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FrameworkTest'
```

## Author

Daniel Salhuana, danielsalhuana@bcp.com.pe

## License

FrameworkTest is available under the MIT license. See the LICENSE file for more info.
