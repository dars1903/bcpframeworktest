//
//  BCPTestFunction.swift
//  FrameworkTest
//
//  Created by Daniel Salhuana on 7/10/21.
//

public class BCPTestFunction {
    
    public init() {}
    
    public func load(message: String) {
        print("El mensaje a mostrar es \(message)")
    }
    
}
